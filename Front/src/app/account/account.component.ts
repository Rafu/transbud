import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../service/ajax.service';
import { Operation } from '../entity/operation';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  operations:Operation[] = [];
  public accounts:Array<Account> = [];
  newOperation:Partial<Operation> = {"amount":0}; 

  public id:number = 0;

  constructor(route: ActivatedRoute, private service:AjaxService) {
    route.params.subscribe(data => this.id = +data['id']);
   }

 
  ngOnInit() {

    this.service.getAllAccount().subscribe(
      data => this.accounts = data,
      error => console.error(error.message),
      () => console.info("findAll() account success")
  
    )
    
    this.service.getAllOperationByAccount(this.id).subscribe(
      data => this.operations = data,
      error => console.error(error.message),
      () => console.info("findAll() operation success")
    )
  }

  public add(operation){
    this.service.addOperation(operation, this.id).subscribe(
      ()=>this.ngOnInit(),
      error => console.error(error.message),
      ()=>console.info("add on operation name:${operation.name}")
    )
  }

  public delete(operation){
    this.service.deleteOperation(operation, this.id).subscribe(
      ()=>this.ngOnInit()
    )
  }
  

}
