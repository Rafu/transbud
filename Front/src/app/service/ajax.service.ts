import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Operation } from '../entity/operation';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  private operationUrl :string = 'http://localhost:8080/operation/';
  private accountUrl  = 'http://localhost:8080/user/account/';


  constructor(private http:HttpClient) { }
  getAllOperation(): Observable<Operation[]> {
    return this.http.get<Operation[]>(this.operationUrl)
  }

  addOperation(operation:Operation, id):Observable<Operation>{
    return this.http.post<Operation>(`${this.accountUrl}${id}/operation`,operation);
  }

  deleteOperation(operation:Operation, idAccount:number):Observable<any>{
    return this.http.delete(`${this.accountUrl}${idAccount}/operation/${operation.id}`);
  }

  getAllOperationByAccount(id){
    return this.http.get<Operation[]>(`http://localhost:8080/user/account/${id}/operation`);
  }

  public getAllAccount():Observable<Account[]>
  {
    return this.http.get<Account[]>(this.accountUrl);
  }

}
