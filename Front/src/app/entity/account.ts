export interface Account {
  id?:number;
  name:string;
  bank:string;
  balance:number;
}
