export interface Operation {
  
  id:number;
  amount:number;
  account:Account;
}
