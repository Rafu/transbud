<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Operation;
use App\Entity\Account;

class OperationFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tab = [123, 456, -12345, 12, 61, -123456, 5];
        $account = new Account();

        foreach ($tab as $key => $value) {
            $operation = new Operation();
            
            $operation->setAmount($value);
            
            $account->setBank("Piggy Bank");
            $account->setName("Personal Account");
            $account->setBalance(16);

            $operation->setAccount($account);

            $manager->persist($account);
            $manager->persist($operation);


        }
        $manager->flush();
        

    }
}
