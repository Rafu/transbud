<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\OperationRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Operation;
use Symfony\Component\HttpFoundation\JsonResponse;

class OperationController extends Controller
{

    private $serializer;

    const NORMALIZER_FORMAT = ['attributes' => ['id', 'amount']];

    public function __construct()
    {
        $encoder = new JsonEncoder();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer([$normalizer], [$encoder]);

    }

    /**
     * @Route("/", methods = {"GET"})
     */
    public function findAllOperation(OperationRepository $repo)
    {

        $list = $repo->findAll();
        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);
        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
    }

    /**
     * @Route("/user/account/{id}/operation", methods = {"GET"})
     */

    public function findbyAccountId(OperationRepository $repo, $id)
    {

        $list = $repo->findby(
            ["account" => $id]
        );
        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);
        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
    }

}