<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\AccountRepository;
use App\Repository\OperationRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Account;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Operation;

/**
 * @Route("/user/account", name="account")
 */

class AccountController extends Controller
{

    private $serializer;

    const NORMALIZER_FORMAT = ['attributes' => ['id', 'bank', 'name', 'balance']];

    public function __construct()
    {
        $encoder = new JsonEncoder();

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer([$normalizer], [$encoder]);

    }

    /**
     * @Route("/", methods = {"GET"})
     */
    public function findAll(AccountRepository $repo)
    {

        $list = $repo->findAll();
        $data = $this->serializer->normalize($list, null, [self::NORMALIZER_FORMAT]);
        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));

    }


    /**
     * @Route("/{id}/operation", name="new_operation", methods={"POST"})
     */
    public function addOperation(Request $request, int $id, AccountRepository $repo, OperationRepository $opeRepo)
    {
        $manager = $this->getDoctrine()->getManager();
        $content = $request->getContent();
        $operation = $this->serializer->deserialize($content, Operation::class, "json");
        $account = $repo->find($id);
        $account->addOperation($operation);
        $account->computeBalance($operation);
        $manager->persist($account);
        $manager->flush();

        $data = $this->serializer->normalize($account, null, ['attributes' => ['id', 'amount', 'account']]);


        return JsonResponse::fromJsonString($this->serializer->serialize($data, "json"));
    }

    /**
     * @Route("/{idAccount}/operation/{id}", name="remove_operation", methods={"DELETE"})
     */

    public function removeOperation(int $idAccount, AccountRepository $repo, OperationRepository $opeRepo, int $id)
    {
        $manager = $this->getDoctrine()->getManager();


        $operation = $opeRepo->find($id);

        $account = $repo->find($idAccount);
        $account->computeBalance($operation, true);

        $manager->remove($operation);
        $manager->persist($account);
        $manager->flush();

        return new Response(204);
    }

}

    